﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnDCombat
{
    class Entity
    {
        string name;
        int size;
        List<String> entityType;
        int challengeRating;
        int armorClass;
        int healthPoints;               // For NPCs HP = Challenge Rating x (Hit Dice + Constitution Modifier) 
        int[] movement = new int[5];    // Movement consists of General Movement, Swimming, Burrowing, Climbing, Flying
        int[] mainStats = new int[6];   // Stats consist of strength, dexterity, contitution, intelligence, wisdom, charisma
        int[] savingThrows = new int[6];// SavingThrow = Modifier + Proficiency
        List<object> spells;
        List<object> proficiencies;
        List<object> senses;
        List<object> miscAbilities;

    }
}
